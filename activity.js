// new collection of users.

db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76.0,
		"email": "stephenhawking@mail.com",
		"department": "HR"
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82.0,
		"email": "neilarmstrong@mail.com",
		"department": "HR"
	},
	{
		"firstName": "Bill",
		"lastName": "Gates",
		"age": 65.0,
		"email": "billgates@mail.com",
		"department": "Operations"
	},
	{
		"firstName": "Jane",
		"lastName": "Doe",
		"age": 21.0,
		"email": "janedoe@mail.com",
		"department": "HR"
	}
])


// Find users (firstName and lastName)

db.users.find(
	{
		$or:[
			{"firstName": {
				$regex: 'S',
				$options: '$i'
				}
			},
			{"lastName": {
				$regex: 'D',
				$options: '$i'
				}
			}
		]
	},
	{
		"_id": 0,
		"firstName": 1,
		"lastName": 1
	}

)


// Find Users (HR department)

db.users.find({
	$and: [
	{
		"department": "HR"
	},
	{
		"age":{
			$gte: 70
		}
	}
	]
})


// Find users (with letter e in firstName and has anAge less than or equal to 30)

db.users.find({
	$and:[
		{"firstName": {
			$regex: 'E',
			$options: '$i'
			}
		},
		{"age": {
			$lte: 30
			}
		}
	]
})